<?php

require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep  = new animal("shaun");

echo "<h2>Release 0</h2>";
echo "name : $sheep->name<br>";
echo "legs : $sheep->legs<br>";
echo "cold blooded : ". $sheep ->cold_blooded;

$sungokong = new Ape("kera sakti");
echo "<br><br><h2>Release 1</h2>";
$kodok = new Frog("buduk");
echo "name : ". $kodok->name."<br>";
echo "legs : ". $kodok->legs."<br>";
echo "cold blooded : ". $kodok->cold_blooded."<br>";
echo $kodok->jump();
echo "<br><br>";

echo "name : ". $sungokong->name."<br>";
echo "legs : ". $sungokong->legs."<br>";
echo "cold blooded : ". $sungokong->cold_blooded."<br>";
echo $sungokong->yell();

